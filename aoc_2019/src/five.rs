use crate::intcode::*;

pub fn run(input: String) {
    let ret = part_one(input);
    println!("Part one {ret}");
}

fn part_one(input: String) -> u32 {
    32
}
