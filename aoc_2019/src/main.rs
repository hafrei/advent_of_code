use aoc_2019::{fileload, five, four, one, three, two};
use std::{env, process::ExitCode};

fn main() -> ExitCode {
    let (day, contents) = match env::args().nth(1) {
        None => {
            println!("Can't do something with nothing! Give me a day to run!");
            return ExitCode::FAILURE;
        }
        Some(d) => match fileload::read_input(d.to_lowercase()) {
            Ok(file) => file,
            Err(e) => {
                println!("Error occurred: {e:?}");
                return ExitCode::FAILURE;
            }
        },
    };

    //Remember: Some(n) => Ok(*n) is valid

    match day.as_str() {
        "one" => one::run(contents),
        "two" => two::run(contents),
        "three" => three::run(contents),
        "four" => four::run(contents),
        "five" => five::run(contents),
        _ => {} //Noop, many checks will have made it so
    }
    ExitCode::SUCCESS
}
