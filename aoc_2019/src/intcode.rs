pub struct Intcode(Vec<i32>);

impl Intcode {
    pub fn new(vals: Vec<i32>) -> Self {
        Self(vals)
    }
    pub fn new_from_string(vals: String) -> Self {
        Self(
            vals.split(',')
                .filter_map(|l| l.parse::<i32>().ok())
                .collect::<Vec<i32>>(),
        )
    }
    pub fn run(&mut self) {
        let mut opr: usize = 0;
        loop {
            match self.0.get(opr) {
                Some(&1) => {
                    let pos_1 = self.get_value_usize(opr + 1);
                    let pos_2 = self.get_value_usize(opr + 2);
                    let pos_3 = self.get_value_usize(opr + 3);
                    let swaps = self.get_value_i32(pos_1) + self.get_value_i32(pos_2);
                    self.swap_position_with_value(pos_3, swaps);
                    opr += 4;
                }
                Some(&2) => {
                    let pos_1 = self.get_value_usize(opr + 1);
                    let pos_2 = self.get_value_usize(opr + 2);
                    let pos_3 = self.get_value_usize(opr + 3);
                    let swaps = self.get_value_i32(pos_1) * self.get_value_i32(pos_2);
                    self.swap_position_with_value(pos_3, swaps);
                    opr += 4;
                }
                Some(&3) => opr += 2,
                Some(&4) => opr += 2,
                Some(&99) => break,
                Some(_) => {} //Noop
                None => opr = 0,
            }
        }
    }
    fn determine_mode(opr: i32) {
        //ABCDE
        // 1002
        let mut process: Vec<i32> = opr
            .to_string()
            .as_mut_str()
            .chars()
            .filter_map(|x| char::to_digit(x, 10).take())
            .map(|x| x as i32)
            .collect();
        let length = process.len();
        if length < 5 {
            process = process.iter().cloned().rev().collect::<Vec<i32>>();
            for _ in 0..5 - length {
                process.push(0);
            }
        }
        //Flip the vec again now that all spaces are filled
        process = process.into_iter().rev().collect();
        process.rotate_right(2);
        //Remember to check the original opr for negativity
        // Now [D,E,A,B,C]
        let (opcode, a, b, c) = parse_codes(process.split_at(2));
    }
    fn parse_codes(left_side: &[i32], right_side: &[i32]) -> (i32, i32, i32, i32) {
        let ls = 
    }
    fn get_value_usize(&self, u: usize) -> usize {
        self.0[u] as usize
    }
    pub fn get_value_i32(&self, u: usize) -> i32 {
        self.0[u]
    }
    pub fn swap_position_with_value(&mut self, position: usize, value: i32) {
        self.0[position] = value
    }
}

#[cfg(test)]
mod tests {
    use crate::intcode::Intcode;
    #[test]
    fn day_two_test_one() {
        let program = vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50];
        let mut intcode = Intcode::new(program);
        intcode.run();
        let result = intcode.get_value_i32(0_usize);
        assert_eq!(result, 3500_i32);
    }
    #[test]
    fn day_five_test_one() {
        let program = vec![1002, 4, 3, 4, 33];
        let mut intcode = Intcode::new(program);
        intcode.run();
    }
}
