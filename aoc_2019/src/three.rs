use std::iter::zip;

pub fn run(input: String) {
    println!("Go off and do something for the next 40 minutes. Thank me later.");
    let res = part_one(input.clone());
    println!("Part one {res}");
    let res = part_two(input);
    println!("Part two {res}");
}

fn part_two(input: String) -> i32 {
    let (wire_one, wire_two) = Wire::two_new(input, true);
    println!("Finding the intersections");
    wire_one.find_dist_from_stepped(wire_two)
}

fn part_one(input: String) -> i32 {
    let (wire_one, wire_two) = Wire::two_new(input, false);
    println!("Finding the intersections");
    let inters = wire_one.find_sorted_intersections(wire_two);
    let _ = inters.len();
    inters.get_distance()
}

#[derive(Debug)]
pub struct Wire(Vec<Point>);

impl Wire {
    fn two_new(input: String, with_steps: bool) -> (Wire, Wire) {
        let (w_o, w_t) = input.split_at(input.find('\n').unwrap());
        (
            Wire::new(w_o.to_string(), with_steps),
            Wire::new(w_t.to_string(), with_steps),
        )
    }
    fn new(input: String, with_steps: bool) -> Self {
        let mut ret = Vec::new();
        let mut start = Point::default(with_steps);
        input
            .split(',')
            .map(|x| Wire::separate(x.to_string()))
            .for_each(|(d, l)| {
                ret.append(&mut Wire::length(start, d, l));
                start = *ret.last().unwrap();
            });
        Self(ret)
    }
    fn from_vec(input: Vec<Point>) -> Self {
        Self(input)
    }

    fn length(start: Point, dir: String, len: i32) -> Vec<Point> {
        let mut next_step = start.step;
        let mut ret = Vec::new();
        match dir.as_str() {
            "U" => {
                let end = start.y + len;
                for y in start.y..=end {
                    ret.push(Point::from(start.x, y, next_step));
                    next_step = do_step(next_step);
                }
            }
            "D" => {
                let end = start.y - len;
                for y in (end..=start.y).rev() {
                    ret.push(Point::from(start.x, y, next_step));
                    next_step = do_step(next_step);
                }
            }
            "L" => {
                let end = start.x - len;
                for x in (end..=start.x).rev() {
                    ret.push(Point::from(x, start.y, next_step));
                    next_step = do_step(next_step);
                }
            }
            "R" => {
                let end = start.x + len;
                for x in start.x..=end {
                    ret.push(Point::from(x, start.y, next_step));
                    next_step = do_step(next_step);
                }
            }
            &_ => panic!("What direction do you think you're going in?"),
        }
        ret
    }

    fn separate(l: String) -> (String, i32) {
        let (dir, dist) = l.trim().split_at(1);
        (dir.trim().to_string(), dist.parse::<i32>().unwrap())
    }

    fn contains(&self, point: &Point) -> bool {
        self.0.contains(point)
    }

    fn find_sorted_intersections(&self, other: Wire) -> Wire {
        let mut new_wire: Vec<Point> = self
            .0
            .iter()
            .cloned()
            .filter(|x| other.contains(x))
            .collect();
        //ONE MEEEEEEELLION YEARS LATER
        new_wire.sort_by_key(|a| a.sum());
        Wire::from_vec(new_wire)
    }

    //Yikes this is bad but if it works OH WELL >_>
    fn find_dist_from_stepped(&self, other: Wire) -> i32 {
        let mut res = 0;
        let mut new_wire_one: Vec<Point> = self
            .0
            .iter()
            .cloned()
            .filter(|x| other.contains(x))
            .collect();
        new_wire_one.sort_by_key(|a| a.sum());

        let mut new_wire_two: Vec<Point> = other
            .get_steps()
            .iter()
            .cloned()
            .filter(|x| new_wire_one.contains(x))
            .collect();
        //ONE MEEEEEEELLION YEARS LATER
        new_wire_two.sort_by_key(|a| a.sum());

        println!("Here they all are in case one doesn't work");
        let mut first = true;
        for (a, b) in zip(new_wire_one, new_wire_two).rev() {
            let a_s = a.step.unwrap();
            let b_s = b.step.unwrap();
            let comp = a_s + b_s;
            println!("{a_s} + {b_s}  = {}", &comp);
            if first {
                res = comp;
                first = false;
            } else if comp.eq(&0) {
                continue;
            } else if res > comp {
                res = comp;
            }
        }
        res
    }

    fn len(&self) -> usize {
        for p in &self.0 {
            println!("{p:?} = {}", p.x.abs() + p.y.abs());
        }
        self.0.len()
    }

    fn get_distance(&self) -> i32 {
        //Because self.0[0] is {0,0}
        self.0[1].sum()
    }

    fn get_steps(&self) -> Vec<Point> {
        self.0.clone()
    }
}

fn do_step(step: Option<i32>) -> Option<i32> {
    step.map(|n| n + 1)
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd)]
struct Point {
    x: i32,
    y: i32,
    step: Option<i32>,
}

impl Point {
    fn default(is_step: bool) -> Self {
        if is_step {
            Self {
                x: 0,
                y: 0,
                step: Some(0),
            }
        } else {
            Self {
                x: 0,
                y: 0,
                step: None,
            }
        }
    }
    fn from(x: i32, y: i32, step: Option<i32>) -> Self {
        Self { x, y, step }
    }

    fn sum(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

#[cfg(test)]
mod tests {
    use crate::three::Wire;
    #[test]
    fn day_three_test_one() {
        let is_step = false;
        let wire_one = Wire::new("R8,U5,L5,D3".to_string(), is_step);
        let wire_two = Wire::new("U7,R6,D4,L4".to_string(), is_step);

        let res_wire = wire_one.find_sorted_intersections(wire_two);
        let result = res_wire.get_distance();
        let _ = res_wire.len();

        assert_eq!(result, 6_i32)
    }

    #[test]
    fn day_three_test_two() {
        let is_step = false;
        let wires = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83";
        let (w_o, w_t) = match wires.find('\n') {
            Some(n) => wires.split_at(n),
            None => panic!("What :D"),
        };

        let wire_one = Wire::new(w_o.to_string(), is_step);
        let wire_two = Wire::new(w_t.to_string(), is_step);

        let res_wire = wire_one.find_sorted_intersections(wire_two);
        let _ = res_wire.len();
        let result = res_wire.get_distance();

        assert_eq!(result, 159_i32);
    }

    #[test]
    fn day_three_test_three() {
        let is_step = false;
        let wires =
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7";

        let (wire_one, wire_two) = Wire::two_new(wires.to_string(), is_step);
        let result_wire = wire_one.find_sorted_intersections(wire_two);

        let _ = result_wire.len();
        let result = result_wire.get_distance();

        assert_eq!(result, 135_i32)
    }

    #[test]
    fn day_three_part_two_test_one() {
        let is_step = true;
        let wire_one = Wire::new("R8,U5,L5,D3".to_string(), is_step);
        let wire_two = Wire::new("U7,R6,D4,L4".to_string(), is_step);

        let result = wire_one.find_dist_from_stepped(wire_two);

        assert_eq!(result, 30_i32)
    }

    #[test]
    fn day_three_part_two_test_two() {
        let is_step = true;
        let wires = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83";

        let (wire_one, wire_two) = Wire::two_new(wires.to_string(), is_step);
        let result = wire_one.find_dist_from_stepped(wire_two);

        assert_eq!(result, 610_i32)
    }

    #[test]
    fn day_three_part_two_test_three() {
        let is_step = true;
        let wires =
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7";

        let (wire_one, wire_two) = Wire::two_new(wires.to_string(), is_step);
        let result = wire_one.find_dist_from_stepped(wire_two);

        assert_eq!(result, 410_i32)
    }
}
