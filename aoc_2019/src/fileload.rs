use std::{env, fs::File, io::Read};

#[derive(Debug)]
pub enum FileLoadError {
    IOError(std::io::Error),
    FileNotFound,
    DayNotReady,
}

impl From<std::io::Error> for FileLoadError {
    fn from(e: std::io::Error) -> FileLoadError {
        FileLoadError::IOError(e)
    }
}

const INPUTS_DIR: &str = "inputs";

pub fn read_input(day: String) -> Result<(String, String), FileLoadError> {
    let proj_file = format!("{}.rs", &day);
    let mut contents = String::new();
    let mut root = env::current_dir()?;

    root.push(proj_file);

    if !root.is_file() {
        return Err(FileLoadError::DayNotReady);
    }

    root.pop();
    root.pop();
    root.push(INPUTS_DIR);
    root.push(&day);

    if root.is_file() {
        let mut f = File::open(root)?;
        f.read_to_string(&mut contents)?;
    } else {
        return Err(FileLoadError::FileNotFound);
    }
    Ok((day, contents))
}
