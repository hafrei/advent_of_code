use crate::intcode::Intcode;

pub fn run(input: String) {
    let res = part_one(input.clone());
    println!("Part one: {res}");
    let res = part_two(input);
    println!("Part two: {res}");
}

fn part_one(input: String) -> i32 {
    let mut intcode = Intcode::new_from_string(input);
    intcode.swap_position_with_value(1, 12);
    intcode.swap_position_with_value(2, 2);
    intcode.run();
    intcode.get_value_i32(0_usize)
}

fn part_two(input: String) -> i32 {
    let target = 19690720;
    let mut noun = 0;
    let mut verb = 0;

    'go: for n in 0..=99 {
        for v in 0..=99 {
            let mut intcode = Intcode::new_from_string(input.clone());
            intcode.swap_position_with_value(1, n);
            intcode.swap_position_with_value(2, v);
            intcode.run();
            if intcode.get_value_i32(0_usize) == target {
                noun = n;
                verb = v;
                break 'go;
            }
        }
    }
    100 * noun + verb
}
