pub fn run(input: String) {
    let res_1 = part_one(input.clone());
    println!("Part one: {res_1}");
    let res_2 = part_two(input);
    println!("Part two: {res_2}");
}

fn part_one(input: String) -> f32 {
    let mut sum = 0.0;
    for i in input.lines() {
        sum += calculate(i.parse::<f32>().expect("Invalid Input in Day One Part One"));
    }
    sum
}

fn part_two(input: String) -> f32 {
    let mut sum = 0.0;
    for i in input.lines() {
        let mut working = i.parse::<f32>().expect("Invalid input in Day One Part Two");
        loop {
            let res = if calculate(working).gt(&0.0) {
                calculate(working)
            } else {
                break;
            };
            working = res;
            sum += res;
        }
    }
    sum
}

fn calculate(f: f32) -> f32 {
    (f / 3.0).floor() - 2.0
}

#[cfg(test)]
mod tests {
    use crate::one::calculate;
    #[test]
    fn part_one_test_one() {
        let result = calculate(12.0);
        assert_eq!(result, 2.0);
    }

    #[test]
    fn part_two_test_one() {
        let mut working: f32 = 14.0;
        let mut sum: f32 = 0.0;
        loop {
            let res = if calculate(working).gt(&0.0) {
                calculate(working)
            } else {
                break;
            };
            working = res;
            sum = sum + res;
        }
        assert_eq!(sum, 2.0);
    }
}
