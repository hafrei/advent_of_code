use std::ops::RangeInclusive;

pub fn run(input: String) {
    let range = prep_range(input);
    let ret = part_one(range.clone());
    println!("Part one is {ret}");
    let ret = part_two(range);
    println!("Part two is {ret}");
}

fn part_two(range: RangeInclusive<u32>) -> u32 {
    let mut counter = 0;
    for x in range {
        let digits = num_to_vec(x);
        if only_increase(&digits) && has_dupe_part_two(digits) {
            counter += 1;
        }
    }
    counter
}

fn part_one(range: RangeInclusive<u32>) -> u32 {
    let mut counter = 0;
    for x in range {
        let digits = num_to_vec(x);
        if only_increase(&digits) && has_dupe(digits) {
            counter += 1;
        }
    }
    counter
}

fn num_to_vec(i: u32) -> Vec<u32> {
    i.to_string()
        .as_mut_str()
        .chars()
        .filter_map(|x| char::to_digit(x, 10).take())
        .collect()
}

fn has_dupe(mut i: Vec<u32>) -> bool {
    let i_l = i.len();
    i.dedup();
    i_l != i.len()
}

fn has_dupe_part_two(i: Vec<u32>) -> bool {
    for n in i.iter().cloned() {
        let comp = i.iter().filter(|x| **x == n).count();
        if comp == 2 {
            return true;
        }
    }
    false
}

fn only_increase(i: &[u32]) -> bool {
    let compares = 5;
    let mut compared = 0;

    for w in i.windows(2) {
        if w[0].cmp(&w[1]).is_le() {
            compared += 1;
        }
    }

    compared == compares
}

fn prep_range(mut input: String) -> RangeInclusive<u32> {
    let dash = input.find('-').unwrap();
    let _ = input.remove(dash);
    let (ls, rs) = input.as_str().split_at(dash);
    ls.trim().parse::<u32>().expect("Not a desired left side")
        ..=rs.trim().parse::<u32>().expect("Not a desired left side")
}
#[cfg(test)]
mod tests {
    use crate::four::{has_dupe, has_dupe_part_two, num_to_vec, only_increase, prep_range};
    #[test]
    fn part_one_test_zero() {
        let input = "125730-579381".to_string();
        let res = prep_range(input);
        assert_eq!(res, (125730..=579381))
    }
    #[test]
    fn part_one_test_one() {
        let ones = 111111;
        let result = has_dupe(num_to_vec(ones));
        //True in part one, false in part two
        assert_eq!(result, true);
    }

    #[test]
    fn part_one_test_two() {
        let has_decrease = 223450;
        let result = only_increase(&num_to_vec(has_decrease));
        assert_eq!(result, false);
    }
    #[test]
    fn part_one_test_three() {
        let no_double = 123789;
        let result = has_dupe(num_to_vec(no_double));
        assert_eq!(result, false);
    }
    #[test]
    fn part_two_test_one() {
        let input = 112233;
        let result = has_dupe_part_two(num_to_vec(input));
        assert_eq!(result, true);
    }
    #[test]
    fn part_two_test_two() {
        let input = 123444;
        let result = has_dupe_part_two(num_to_vec(input));
        assert_eq!(result, false);
    }
    #[test]
    fn part_two_test_three() {
        let input = 111122;
        let result = has_dupe_part_two(num_to_vec(input));
        assert_eq!(result, true);
    }
}
